'use strict';

!(function () {
    const icon = document.querySelector('.icon');
    const mandarin = document.querySelector('#mandarin');
    const grinch = document.querySelector('#grinch');
    const mandarinAmountElement = document.querySelector('#mandarinAmount');
    const gameTime = document.querySelector('#gameTime');
    const startButton = document.querySelector('#startButton');
    const restartButton = document.querySelector('#restartButton');
    const tryAgainBtn = document.querySelector('#tryAgainBtn');
    const gameCanvas = document.querySelector('.game-canvas');
    const gameCanvasWidth = gameCanvas.offsetWidth;
    const gameCanvasHeight = gameCanvas.offsetHeight;
    const easyLevel = document.querySelector('#easyLevel');
    const mediumLevel = document.querySelector('#mediumLevel');
    const hardLevel = document.querySelector('#hardLevel');
    const whoAreYouLevel = document.querySelector('#whoAreYouLevel');

    let gameInterval;
    let mandarinInterval;
    let grinchIntervalDelay;
    let mandarinCount = 0;
    let gameDuration = 30;
    let interval = 1500;

    mandarin.addEventListener('click', () => {
        mandarinCount++;
        mandarinAmountElement.textContent = `You caught ${mandarinCount} mandarins`;
        setIconPosition();
        clearInterval(mandarinInterval);
        mandarinInterval = setInterval(setIconPosition, 1000);
    });

    grinch.addEventListener('click', () => {
        gameDuration = 30;
        mandarinCount = 0;
        gameTime.textContent = `GAME OVER!`;
        icon.style.display = 'none';
        clearInterval(gameInterval);
        clearInterval(mandarinInterval);
        getTryAgain();
    });

    tryAgainBtn.addEventListener('click', () => {
        setButtonActivity();
        getTryAgain();
    });

    function getTryAgain() {
        startButton.classList.toggle('btn-display-none');
        restartButton.classList.toggle('btn-display-none');
        tryAgainBtn.classList.toggle('btn-display-none');
    }

    function gameCount() {
        if (gameDuration === 1) {
            icon.style.display = 'none';
            gameTime.textContent = `TIME IS OVER!`;
            clearInterval(gameInterval);
            clearInterval(mandarinInterval);
            getTryAgain();
            return;
        }
        gameDuration--;
        gameTime.textContent = `${gameDuration} seconds left`;
    }

    function resetGame() {
        gameDuration = 30;
        mandarinCount = 0;
        mandarinAmountElement.textContent = `You caught ${mandarinCount} mandarins`;
        gameTime.textContent = `${gameDuration} seconds left`
    }

    function setButtonActivity() {
        startButton.toggleAttribute('disabled');
        startButton.classList.toggle('disable-button');
        restartButton.toggleAttribute('disabled');
        restartButton.classList.toggle('disable-button');
    }

    function setIconPosition() {
        let iconWidth = icon.offsetWidth;
        let iconHeight = icon.offsetHeight;
        let playingFieldWidth = gameCanvasWidth - iconWidth;
        let playingFieldHeight = gameCanvasHeight - iconHeight;
        icon.style.left = Math.round(Math.random() * playingFieldWidth) + 'px';
        icon.style.top = Math.round(Math.random() * playingFieldHeight) + 'px';

        if (gameDuration === grinchIntervalDelay) {
            mandarin.style.display = 'none';
            grinch.style.display = 'block';
            grinchIntervalDelay = Math.round(Math.random() * gameDuration);
        } else {
            mandarin.style.display = 'block';
            grinch.style.display = 'none';
        }


    }
    function chooseLevel() {
        easyLevel.addEventListener('click', () => {
            interval = 1500;
        });
        mediumLevel.addEventListener('click', () => {
            interval = 1000;
        });
        hardLevel.addEventListener('click', () => {
            interval = 800;
        });
        whoAreYouLevel.addEventListener('click', () => {
            interval = 300;
        });
        return interval;
    }

    interval = chooseLevel();

    function startGame() {
        icon.style.display = 'block';
        setIconPosition();
        resetGame();
        grinchIntervalDelay = Math.round(Math.random() * gameDuration);
        mandarinInterval = setInterval(setIconPosition, interval);
        gameInterval = setInterval(gameCount, 1000);
        console.log(interval);
    }

    startButton.addEventListener('click', () => {
        setButtonActivity();
        startGame();
    });

    restartButton.addEventListener('click', () => {
        clearInterval(gameInterval);
        clearInterval(mandarinInterval);
        startGame();
    });
}());