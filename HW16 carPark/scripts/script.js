'use strict';

!(function () {
    function createCar(color = 'black', engineCapacity = 1.4, tankVolume = 40) {
        let _engineCapacity = engineCapacity;
        let _tankVolume = tankVolume;
        let _setInterval;
        let fuelConsumption;

        function _inputValidation() {
            if (!_isNumber(engineCapacity)) {
                console.warn(`engineCapacity createCar parameter must be a Number!`);
                return false;
            }
            if (engineCapacity < 1.1 || engineCapacity > 5) {
                console.warn(`engineCapacity createCar parameter must be between 1.1 and 5 inclusive!`);
                return false;
            }
            if (!_isNumber(tankVolume)) {
                console.warn(`tankVolume createCar parameter must be a Number!`);
                return false;
            }
            if (tankVolume < 20 || tankVolume > 100) {
                console.warn(`tankVolume createCar parameter must be between 20 and 100 inclusive!`);
                return false;
            }
            if (typeof color !== 'string') {
                console.warn(`color createCar parameter must be a Srting!`);
                return false;
            }
            return true;
        }

        if (!_inputValidation()) {
            return;
        }

        function _isNumber(number) {
            return !isNaN(parseFloat(number)) && isFinite(number);
        }

        function _checkGasInput(isFull, gasVolume) {
            if (typeof isFull !== 'boolean') {
                console.warn(`isFull addGas parameter must be a Boolean!`);
                return false;
            }
            if (!isFull && !_isNumber(gasVolume)) {
                console.warn(`gasVolume addGas parameter must be a Number!`);
                return false;
            }
            return true;

        }

        function _checkGasVolume(gasVolume) {
            return (car.gasVolume + gasVolume) <= _tankVolume && gasVolume >= 0;
        }

        function _checkGasExisting() {
            return car.gasVolume !== 0;
        }

        function _checkSpeedDistance(speed, distance) {
            if (!_isNumber(speed) && !_isNumber(distance)) {
                console.warn(`speed and distance drive parameters must be a Number!`);
                return false;
            }
            if (speed <= 0 || speed > 200) {
                console.warn(`speed can't be less then 0 and speed cannot exceed 200 km`);
                return false;
            }
            if (distance <= 0) {
                console.warn(`distance can't be less then 0`);
                return false;
            }
            return true;
        }

        function _fuelConsumption(speed) {
            let consumption;
            if (speed < 60) {
                if (_engineCapacity >= 1.1 && _engineCapacity <= 1.8) {
                    consumption = 5 * _engineCapacity;
                } else if (_engineCapacity >= 1.9 && _engineCapacity <= 3.5) {
                    consumption = 5 * _engineCapacity;
                } else if (_engineCapacity >= 3.6 && _engineCapacity <= 5) {
                    consumption = 5 * _engineCapacity;
                }
            } else if (speed >= 60 && speed <= 90) {
                if (_engineCapacity >= 1.1 && _engineCapacity <= 1.8) {
                    consumption = 3 * _engineCapacity;
                } else if (_engineCapacity >= 1.9 && _engineCapacity <= 3.5) {
                    consumption = 3 * _engineCapacity;
                } else if (_engineCapacity >= 3.6 && _engineCapacity <= 5) {
                    consumption = 3 * _engineCapacity;
                }
            } else if (speed > 90) {
                if (_engineCapacity >= 1.1 && _engineCapacity <= 1.8) {
                    consumption = 7 * _engineCapacity;
                } else if (_engineCapacity >= 1.9 && _engineCapacity <= 3.5) {
                    consumption = 7 * _engineCapacity;
                } else if (_engineCapacity >= 3.6 && _engineCapacity <= 5) {
                    consumption = 7 * _engineCapacity;
                }
            }
            return fuelConsumption = consumption / 100;
        }

        const car = {
            color: color,
            gasVolume: 0,
            ignition: false,
            checkGas() {
                return car.gasVolume.toFixed(2);
            },
            addGas(isFull = 'false', gasVolume = 0) {
                if (!_checkGasInput(isFull, gasVolume)) {
                    return;
                }
                if (isFull) {
                    car.gasVolume = _tankVolume;
                } else {
                    if (_checkGasVolume(gasVolume)) {
                        car.gasVolume += gasVolume;
                    } else {
                        console.warn(`Gas volume is to large. You can add ${(_tankVolume - car.gasVolume).toFixed(2)}`);
                    }
                }
            },
            start() {
                if (car.ignition === true) {
                    console.warn(`The car is already running! Do not turn the key in the ignition!`);
                    return;
                }
                clearInterval(_setInterval);
                if (car.gasVolume === 0) {
                    console.warn(`No fuel!`);
                    return;
                }
                car.ignition = true;
                _setInterval = setInterval(() => {
                    if (_checkGasExisting()) {
                        car.gasVolume -= 0.01;
                    } else {
                        car.stop();
                        console.warn(`Fuel run out!`);
                    }
                }, 1000);
            },
            stop() {
                car.ignition = false;
                clearInterval(_setInterval);
            },
            drive(speed, distance) {
                if (car.ignition === false) {
                    console.warn(`Ignition off. To start driving - start the car!`);
                    return;
                }
                if (!_checkSpeedDistance(speed, distance)) {
                    return;
                }
                _fuelConsumption(speed);
                if (car.gasVolume >= distance * fuelConsumption) {
                    car.gasVolume -= distance * fuelConsumption;
                    console.log(`The car covered ${distance} km in ${(distance / speed * 60).toFixed(2)} minutes at a speed of ${speed} km/h, the rest of the gas is ${car.gasVolume.toFixed(2)} liters`);
                } else if (car.gasVolume < distance * fuelConsumption) {
                    console.warn(`In order to travel a distance of ${distance} km you do not have enough fuel!
    You have ${car.gasVolume.toFixed(2)} liters of fuel left. With this amount of fuel, you can drive ${(car.gasVolume.toFixed(2) / fuelConsumption).toFixed(2)} km`);
                }
                car.stop();
            },
        };
        return car;
    }
}());