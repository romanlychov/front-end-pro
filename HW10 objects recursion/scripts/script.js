'use strict';

let obj = {
    a: 'f',
    b: 78,
    c: 'R',
    d: {
        a: {
            a: null,
            b: 'E',
            c: {
                a: true,
                b: 'C',
                c: 'test'
            },
            d: 'U'
        },
        b: {
            a: 'R',
            b: ['S', 4, 6, 'I'],
            c: 0,
        },
        c: ['O'],
        d: null,
        e: 'N'
    }
};

function checkForUpperCase(value) {
    let isUpperCase = (typeof value === 'string' && value.length === 1 && value.match(/[A-Z]/)) ? true : false;
    return isUpperCase;
}

let resultString = '';

function getUpperCaseLetters(obj) {
    for (const [, value] of Object.entries(obj)) {
        if (typeof value === 'string') {
            if (checkForUpperCase(value)) {
                resultString += value;
            }
        } else if (Array.isArray(value)) {
            value.forEach(item => {
                if (checkForUpperCase(item)) {
                    resultString += item;
                }
            });
        } else if (typeof value === 'object' && value) {
            getUpperCaseLetters(value);
        }
    }
    return resultString;
}
console.log(getUpperCaseLetters(obj));