'use strict';

let accordions = document.querySelectorAll('.acc-style');
let subAccordion = document.querySelectorAll('.sub-acc-style');

accordions.forEach(item => {
    item.addEventListener('click', openAccordion);
});

function openAccordion(event) {
    let sibling = event.target.nextElementSibling;
    // if (sibling) {
    //     for (const i of subAccordion) {
    //         if (i.classList.contains('active-sub-acc-style')) {
    //             i.classList.remove('active-sub-acc-style');
    //         }
    //     }
    //     sibling.classList.add('active-sub-acc-style');
    // }
    if (sibling) {
        if (sibling.classList.contains('active-sub-acc-style')) {
            sibling.classList.remove('active-sub-acc-style');
        } else {
            for (const i of subAccordion) {
                if (i.classList.contains('active-sub-acc-style')) {
                    i.classList.remove('active-sub-acc-style');
                }
            }
            sibling.classList.add('active-sub-acc-style');
        }
    }
}