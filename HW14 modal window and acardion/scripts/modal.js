'use strict';

let openBtn = document.querySelector('#open-btn');
let filterContainer = document.querySelector('.filter-container');
let closeBtn = document.querySelector('.close-btn');


openBtn.addEventListener('click', () => {
    filterContainer.classList.remove('display-none');
});

let closeWindow = event => {
    if (event.target === filterContainer || event.target === closeBtn) {
        filterContainer.classList.add('display-none');
    }
};

filterContainer.addEventListener('click', closeWindow);