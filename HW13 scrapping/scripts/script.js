'use strict';

function isNumber(number) {
    return isNaN(number) || !isFinite(parseFloat(number));
}

function inputValidation(part, chapter, style) {

    if (isNumber(part)) {
        console.warn(`part getParagraphs function must be a number!`);
        return false;
    }

    if (isNumber(chapter)) {
        console.warn(`chapter getParagraphs function must be a number!`);
        return false;
    }

    if (part < 1 || part > 3) {
        console.warn(`part getParagraphs function must be more then 0 and less then 4!`);
        return false;
    }

    if (part === 1) {
        if (chapter < 1 || chapter > 14) {
            console.warn(`chapter getParagraphs function when part = ${part} must be more then 0 and less then 15!`);
            return false;
        }
    } else if (part === 2) {
        if (chapter < 1 || chapter > 6) {
            console.warn(`chapter getParagraphs function when part = ${part} must be more then 0 and less then 7!`);
            return false;
        }
    } else if (part === 3) {
        if (chapter < 1 || chapter > 8) {
            console.warn(`chapter getParagraphs function when part = ${part} must be more then 0 and less then 9!`);
            return false;
        }
    }

    if (typeof style !== 'string' || style !== 'number' && style !== 'dash' && style !== 'current') {
        console.warn(`style getParagraphs function must be String!
style getParagraphs function must be number, dash or current!`);
        return false;
    }
    return true;
}

function getParagraphs(part, chapter, style) {
    if (!inputValidation(part, chapter, style)) {
        return;
    }

    let tab = document.querySelector(`#tab-${part}`);
    let listItem = tab.querySelector(`.list__item:nth-child(${chapter})`);
    let listSubItem = listItem.querySelectorAll(`.list-sub__link`);
    let book = [];

    for (const listSubTitle of listSubItem) {
        book.push(listSubTitle.textContent);
    }

    let result = ``;
    if (style === 'number') {
        for (let i = 0; i < book.length; i++) {
            result += `${i + 1}. ${book[i]}
`;
        }
    } else if (style === 'dash') {
        for (let i = 0; i < book.length; i++) {
            result += `- ${book[i]}
`;
        }
    } else if (style === 'current') {
        for (let i = 0; i < book.length; i++) {
            result += `${chapter}.${i + 1} ${book[i]}
`;
        }
    }
    return result;
}
console.log(getParagraphs(1, 1, 'number'));