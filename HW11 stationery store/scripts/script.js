'use strict';

let stationeryStore = [
    {
        name: 'pen',
        price: 17,
        category: 'pens'
    },
    {
        name: 'pencil',
        price: 10,
        category: 'pens'
    },
    {
        name: 'color pencil',
        price: 13,
        category: 'pens'
    },
    {
        name: 'color paper',
        price: 19,
        category: 'papers'
    },
    {
        name: 'white paper',
        price: 12,
        category: 'papers'
    },
    {
        name: 'paper folder',
        price: 20,
        category: 'folders'
    },
    {
        name: 'folder for notebooks',
        price: 27,
        category: 'folders'
    }
];

function isNumber(massege) {
    let number;
    do {
        number = +prompt(massege, '0');
    } while (isNaN(number) || !isFinite(parseFloat(number)));
    return number;
}

function createNewProduct(item) {
    if (!Array.isArray(item)) {
        console.warn(`item is not Array!`);
        return;
    }
    let newProduct;
    do {
        newProduct = {
            name: prompt(`Enter the name of the new product:`, 'pen'),
            price: isNumber(`Enter the price of the new product`),
            category: prompt(`Enter the category of the new product`, 'pens')
        };
        item.push(newProduct);
    } while (confirm(`Do you want to continue?`));
    return item;
}
// console.log(createNewProduct(stationeryStore));

function priceRangeFilter(item) {
    let minPrice = isNumber(`Enter min price:`);
    let maxPrice = isNumber(`Enter max price:`);
    return item.filter(item => item.price >= minPrice && item.price <= maxPrice);
}
// console.log(priceRangeFilter(stationeryStore));

function filterByProductCategory(item) {
    let category = prompt(`Emter the category of product`, 'pens');
    return item.filter(item => item.category === category);
}
// console.log(filterByProductCategory(stationeryStore));

function numberOfProductsInCategory(item) {
    let resultArr = filterByProductCategory(item);
    return resultArr.length;
}
// console.log(numberOfProductsInCategory(stationeryStore));

function deleteProductByName(item) {
    let deletedProduct = prompt(`Enter the name of the product you want to remove:`, 'pen');
    for (let i = 0; i < item.length; i++) {
        if (item[i].name === deletedProduct) {
            item.splice(item[i], 1);
            break;
        }
    }
    return item;
}
// console.log(deleteProductByName(stationeryStore));

function sortByPriceMinMax(item) {
    let minMaxResult = item.slice();
    minMaxResult = minMaxResult.sort((a, b) => a.price - b.price);
    return minMaxResult;
}
// console.log(sortByPriceMinMax(stationeryStore));

function sortByPriceMaxMin(item) {
    let maxMinResult = item.slice();
    maxMinResult = maxMinResult.sort((a, b) => b.price - a.price);
    return maxMinResult;
}
// console.log(sortByPriceMaxMin(stationeryStore));

function sortAndFilter(item) {
    let newArray = item.slice();
    let selectedSort;
    let selectedFilter;

    do {
        selectedSort = +prompt(`Choose a sort:
        1. Sort MIN to MAX
        2. Sort MAX to MIN
        Enter 1 or 2`)
    } while (isNaN(selectedSort) || !isFinite(parseFloat(selectedSort)) || selectedSort < 1 || selectedSort > 2);

    if (selectedSort === 1) {
        newArray = sortByPriceMinMax(newArray);
    } else if (selectedSort === 2) {
        newArray = sortByPriceMaxMin(newArray);
    }

    do {
        selectedFilter = +prompt(`Choose a filter:
        1. Filter by product category
        2. Price range filter
        Enter 1 or 2`)
    } while (isNaN(selectedFilter) || !isFinite(parseFloat(selectedFilter)) || selectedFilter < 1 || selectedFilter > 2);

    if (selectedFilter === 1) {
        newArray = filterByProductCategory(newArray);
    } else if (selectedFilter === 2) {
        newArray = priceRangeFilter(newArray);
    }

    return newArray;
}
// console.log(sortAndFilter(stationeryStore));

function sumOfPrices(item) {
    let newArray = item.slice();
    let selectedFilter;
    let sum = 0;

    do {
        selectedFilter = +prompt(`Choose a filter:
        1. Filter by product category
        2. Price range filter
        Enter 1 or 2`)
    } while (isNaN(selectedFilter) || !isFinite(parseFloat(selectedFilter)) || selectedFilter < 1 || selectedFilter > 2);

    if (selectedFilter === 1) {
        newArray = filterByProductCategory(newArray);
    } else if (selectedFilter === 2) {
        newArray = priceRangeFilter(newArray);
    }

    for (let i = 0; i < newArray.length; i++) {
        sum += newArray[i].price;
    }

    return sum;
}
// console.log(sumOfPrices(stationeryStore));